#TODO
- Add origin to the AST nodes

- Type inference from function returns and struct members

- Implement semantic verification of if, while, and for loop headers

- Basic C backend

- Add type decorators

- Make sure the dot operator is converted into the pointer operator where necessary

- Add top-level items like imports, namespace declarations, and exports

- Create a linker to parse external source files and produce a single output

- Bootstrap the compiler in Fwum

- Parse index operator and function call with left-value as an expression
